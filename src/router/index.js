import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'


Vue.use(VueRouter)

const routes = [
  {
    path: "/register",
    name: 'Register',
    component: () => import('../views/register/Register.vue')
  },
  {
    path:"/login",
    name: 'Login',
    component: () => import('../views/auth/Login.vue')
  },
  {
    path: '/',
    name: 'App',
    redirect: '/users',
    component : () => import( '../components/Layout.vue'),
    children: [
      {
        path: '/users',
        name: 'Users',
        component: () => import( '../views/users/Users.vue')
      },
      {
        path:'/users/:id/todos',
        name: 'UserTodos',
        
        component: () => import('../views/userTodos/UserTodos.vue')
      },
    ]
  },
  {
    path:'*',
    name :'For0for',
    component: () => import( '../views/for0for/For0for.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const session = localStorage.getItem("isLogged")
  if(session){
    store.dispatch("initSession")
  }
  if (to.name !== 'Login' && !session) next({ name: 'Login' })
  else next()
})
export default router
