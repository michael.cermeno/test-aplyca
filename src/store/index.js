import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    asideToogle: false,
    data: [],
    todos: [],
    authenticated: null,
  },
  mutations: {
    setAsideToggle(state) {
      state.asideToogle = !state.asideToogle
    },
    setData(state, data) {
      state.data = data
    },
    setTodos(state, todos) {
      state.todos = todos
    },
    setNewTodo(state,todo){
      state.todos = [...state.todos,todo]
    },
    setDeleteTodo(state,id){
      state.todos = state.todos.filter((todo) => todo.id !== id)
    },
    setAuthenticated(state,isLogged){
      state.authenticated = isLogged
      localStorage.setItem("isLogged",JSON.stringify(isLogged) )
    },
    setInitSession(state){
     const session = localStorage.getItem("isLogged")
     state.authenticated = session ? JSON.parse(session) : null
    },
    setDeleteSession(state){
      state.authenticated = null
      localStorage.removeItem("isLogged")
    }
  },
  actions: {
    togglerAside({ commit }) {
      commit('setAsideToggle')
    },
    loadData({ commit }) {
      fetch("https://jsonplaceholder.typicode.com/users")
        .then(res => res.json())
        .then(data => {
          commit("setData", data)
      })
    },
    loadTodos({ commit }, id) {
      fetch(`https://jsonplaceholder.typicode.com/todos?userId=${id}`)
        .then(res => res.json())
        .then(todos => {
          commit("setTodos", todos)
        })
    },
     async addNewTodo({commit},body){
      const res = await fetch(`https://jsonplaceholder.typicode.com/users/${body.userId}/todos`,{
        method: 'POST',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify(body)
      })
      const todo = await res.json()
      todo.id = body.id
      commit("setNewTodo",todo)

    },
    deleteTodo({commit},id){
      commit("setDeleteTodo",id)
    },
    login({commit},email){
      commit("setAuthenticated",{email, isLogged:true})
    },
    logOut({commit}){
      commit("setDeleteSession") 
    },
    initSession({commit}){
      commit("setInitSession")
    }
  
  },

})
